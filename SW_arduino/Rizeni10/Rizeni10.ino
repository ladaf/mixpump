#include <Servo.h>
#include "strikacka.h"

Strikacka strik;

void setup() {
  Serial.begin(9600);
   while (!Serial) {
    ;
   }
  pinMode(6, INPUT_PULLUP);
  strik.setInPulse(1000);   // krajni poloha 1
  strik.setOutPulse(1600);  // krajni poloha 2
  // pro hadickove servo se hodi 1000 a 1600
  // pro SRT servokohout se hodi 500 a 2400
  strik.begin(2, 3, 4, 5, 7, 8, 9);
  //prvni ctyri cisla - HMustek
  //pate a seste - limit tlacitka
  //posledni - servo
}

void loop() {
  while(!Serial.available()){
    delay(10);
  }
  int pocet = Serial.parseInt();
  if(pocet < 1 || pocet > 300){
    Serial.println("Sorry vole error!");
  }
  else{
    for (int i = 1; i <= pocet; ++i){
      Serial.print("Pump ");
      Serial.print(i);
      Serial.print(" z ");
      Serial.println(pocet);
      strik.pump();
    }
    Serial.println("hotovo");
  }
}
