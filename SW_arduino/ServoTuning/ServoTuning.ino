#include <Servo.h>

/* Simple for testing/calibrating servos
 * 
 * User inputs time (in uS) manualy to serial console
 * Then checks the servo physically
 */


Servo myservo; 
unsigned long pulse_duration = 1500;    

void setup() {
  Serial.begin(9600);
  myservo.attach(9);
}

void loop() {
  Serial.print("Pulse duration: ");
  Serial.println(pulse_duration);

  while(!Serial.available()){
    delay(42);
  }
  pulse_duration = Serial.parseInt();
  
  
  myservo.writeMicroseconds(pulse_duration);
}
