Mechanical design
===

This folder is for storing drawings of mechanical components.
Many parts are designed to be 3D printed. For most parts, SolveSpcae CAD was used. Both .stl and .step files are usually included for easy 3D printing.



Mechanický návrh
===

V této složce jsou uloženy materiály pro mechanické části.
Některé díly jsou zamýšleny pro 3D tisk. Tyto dály byly nakresleny v programu SolveSpace. Pro jednoduchot jsou zde také .stl i .step soubory.

