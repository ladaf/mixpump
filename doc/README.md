Documentation
===

Documentation for both this project is stored here.

Documentation for used parts is not contained in this repo, only links


##Links
[Servo](http://www.srt-rc.com/data/upload/file/201605/61864b18ad2b898e3ff4b12f48fd2bf4.pdf "SRT DL3017 Servo datasheet")


[H-Bridge IC](https://www.st.com/resource/en/datasheet/l298.pdf "L298n Dual H-Bridge IC datasheet (ST)")


[H-Bridge Module](https://howtomechatronics.com/tutorials/arduino/arduino-dc-motor-control-tutorial-l298n-pwm-h-bridge/ "L298n Module tutorial")
