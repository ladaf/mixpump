MixPump
===
This project is currently in early stage of development. Further ddocumentation is available only in Czech language



MixPump je nástroj (HW i SW) pro přípravu vzorků směsí plynu.

Mechnická stránka sestává z velké stříkačky poháněné DC motorem a závitovou tyčí. Pohyb je omezem pomocí mechanických limitních spínačů. Dále je ke stříkačce připojen trojcestný kohout, který je ovládán pomocí standardního modelářského serva. Celý systém je řízený pomocí mikrokontroléru ATMega328 na desce arduino nano.

Tento repozítář zatím obsahuje kód pro arduino. Dokumentace harwaru (mechanická i elektrická) bude doplněna.

